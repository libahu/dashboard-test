import { Button, Cascader, DatePicker, Form, Input, InputNumber, Radio, Select, Switch, TreeSelect } from "antd";
import { useAppDispatch, useAppSelector } from "../../stores/hooks";
import React, { useEffect, useState } from "react";
import copy_clipboard from '../../assets/images/dashboard/copy_clipboard.svg'

type SizeType = Parameters<typeof Form>[0]['size'];

const Development: React.FC = () => {

	const dispatch = useAppDispatch();
	// const { syncMyAsset } = useMutationSyncMyAsset();
	const [height, setHeight] = useState(0);

	useEffect(() => {
		if (!height) setHeight(window.innerHeight);
		window.addEventListener("resize", () => {
			setHeight(window.innerHeight - 100);
		});
	}, [height]);

	const [componentSize, setComponentSize] = useState<SizeType | 'default'>('default');

	const onFormLayoutChange = ({ size }: { size: SizeType }) => {
		setComponentSize(size);
	};

	return (
		<React.Fragment>
			<div className="col col-content" id="myItemScrollAble" style={{ height: height, overflow: "auto" }}>
				<main className="main-content">
					<div className="header-toolbar pt-4 pb-4 title-develop">
						<h1 className="title-toolbar h2">Development</h1>
					</div>
					<div className="border-develop">
						<div>
							<h4 className="title-develop">API Credentials</h4>
						</div>
						<div className="m-develop">
							<h5 className="m-item-develop">API KEY</h5>
							<div className="m-item-develop">An API Key uniquely identifies you as a partner.</div>
							<div className='m-item-develop row-develop'>
								<div className='text-develop-color'>8bad9361d656302ff50cf8ff8910% </div>
								<img src={copy_clipboard} />
							</div>
						</div>
						<div className="m-develop">
							<h5 className="m-item-develop">API Secret</h5>
							<div className="m-item-develop">An API Secret is to be used to fresh your API Success token.</div>
							<div className='m-item-develop row-develop'>
								<div className='text-develop-color'>8bad9361d656302ff50cf8ff8910% </div>
								<img src={copy_clipboard} />
							</div>
						</div>
						<div className="m-develop">
							<h5 className="m-item-develop">API Secret</h5>
							<div className="m-item-develop">
								An API Secret is to be used to fresh your API Success token.
								An API Secret is to be used to fresh your API Success token
								An API Secret is to be used to fresh your API Success token
								An API Secret is to be used to fresh your API Success token
								An API Secret is to be used to fresh your API Success token
								An API Secret is to be used to fresh your API Success token
								An API Secret is to be used to fresh your API Success token
							</div>
							<div className='m-item-develop row-develop'>
								<div className='text-develop-color'>8bad9361d656302ff50cf8ff8910% </div>
								<img src={copy_clipboard} />
							</div>
						</div>
						<div className="m-develop">
							<h5 className="m-item-develop">API Secret</h5>
							<div className="m-item-develop">An API Secret is to be used to fresh your API Success token.</div>
							<div className='m-item-develop row-develop'>
								<div className='text-develop-color'>8bad9361d656302ff50cf8ff8910% </div>
								<img src={copy_clipboard} />
							</div>
						</div>
					</div>
				</main>
			</div>
		</React.Fragment>
	);
};

export default Development;
