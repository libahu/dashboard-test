import { Button, Checkbox, Input, Select, Space } from "antd";
import { useAppDispatch, useAppSelector } from "../../stores/hooks";
import React, { useEffect, useState } from "react";

const Setting: React.FC = () => {

	const dispatch = useAppDispatch();
	// const { syncMyAsset } = useMutationSyncMyAsset();
	const [height, setHeight] = useState(0);

	useEffect(() => {
		if (!height) setHeight(window.innerHeight);
		window.addEventListener("resize", () => {
			setHeight(window.innerHeight - 100);
		});
	}, [height]);

	const plainOptions = ['Buy', 'Sell'];

	return (
		<React.Fragment>
			<div className="col col-content" id="myItemScrollAble" style={{ height: height, overflow: "auto" }}>
				<main className="main-content">
					<div className="header-toolbar pt-4 pb-4">
						<h1 className="title-toolbar h2">Setting</h1>
					</div>
					<div className="border-setting">
						<div className="m-setting">
							<div className="m-item-setting">Company name</div>
							<Input disabled placeholder="ChainVerse Partner" style={{ maxWidth: 600 }}  className="m-item-setting"/>
						</div>
						<div className="m-setting">
							<div className="m-item-setting">Website</div>
							<Input disabled placeholder="ChainVersePartner@hello.com" style={{ maxWidth: 600 }} className="m-item-setting"/>
						</div>
						<div className="m-setting">
							<div className="m-item-setting">Brand Name</div>
							<Input placeholder="Partner" style={{ maxWidth: 600 }} className="m-item-setting"/>
						</div>
						<div className="m-setting">
							<div className="m-item-setting">Products enabled</div>
							<Checkbox.Group options={plainOptions} defaultValue={['Apple']} className="m-item-setting"/>
						</div>
						<div className="m-setting">
							<div className="m-item-setting">Fee configuration</div>
							<div className="m-item-setting">
								<Space wrap className="m-item-setting">
									<Button type="primary" style={{ borderRadius: 20 }} >Fixed</Button>
									<Button type="default" style={{ borderRadius: 20 }} >Percentage</Button>
								</Space>
							</div>
							<Input
								placeholder="Percentage fee lower than 5%"
								style={{ maxWidth: 600 }}
								addonAfter="%"
								className="m-item-setting"
							/>
						</div>
						<div className="m-setting">
							<div className="m-item-setting">Payment method</div>
							<Select
							m-item-setting
								defaultValue="lucy"
								style={{ width: 600 }}
								// onChange={handleChange}
								options={[
									{ value: 'jack', label: 'Jack' },
									{ value: 'lucy', label: 'Lucy' },
									{ value: 'Yiminghe', label: 'yiminghe' },
									{ value: 'disabled', label: 'Disabled', disabled: true },
								]}
							/>
						</div>
					</div>
				</main>
			</div>
		</React.Fragment>
	);
};

export default Setting;
