import React, { } from 'react';
import { Button, Form, Input } from 'antd';
import bg_img from '../../assets/images/dashboard/bg-img.svg'
import logo_cv from '../../assets/images/dashboard/logo_cv.svg'
import metamask from '../../assets/images/dashboard/metamask.svg'
import { useHistory } from 'react-router-dom';

const Login: React.FC = () => {
	const history = useHistory();
	const handleClick = () => {
		history.push('/');
	}

	return (
		<div className="form-card" style={{ overflow: 'hidden' }}>
			<section className="form-card-page form-card row no-gutters justify-content-md-center">
				<div className="form-card__body col-lg-6 p-5 px-lg-8 ">
					<div className="brand-logo">
						<img
							className="brand-logo-img"
							src={logo_cv}
							alt="ChainVerse"
							title="ChainVerse"
						/>
					</div>
					<section className="form-v1-container">
						<h3 className="login-title">Welcome to ChainVerse Dashboard</h3>
						<Form
							// form={form}
							name="FormData"
							labelCol={{ span: 6 }}
							wrapperCol={{ span: 18 }}
							layout="horizontal"
							scrollToFirstError>
							<Form.Item
								label="Email"
								name="email"
								rules={[
									{
										required: true,
										message: 'Email not empty!',
									},
									{
										whitespace: true,
										message: 'This field is required!',
									},
								]}>
								<Input autoFocus placeholder="Enter email..." />
							</Form.Item>
							<Form.Item
								label="Password"
								name="Password"
								rules={[
									{
										required: true,
										message: 'Password not empty!',
									},
								]}
							>
								<Input autoComplete='' placeholder="Enter password..." type={'password'} />
							</Form.Item>
						</Form>
						<Button
							onClick={() => handleClick()}
							type="primary"
							htmlType="submit"
							className="btn-primary btn-cta ">
							Login
						</Button>
					</section>
				</div>
				<img className="mask-img" src={bg_img} alt="" title="" />
			</section>
			<div className="mask-bg" />
		</div>
	);
};

export default Login;
