import React, { useEffect, useState } from "react";
import { useAppDispatch } from "../../stores/hooks";
import { getAddressWallet } from "../../stores/userSlice";
import { useAppSelector } from "../../stores/hooks";
import { Row, Col } from "reactstrap";
import ChartStack from "../../components/ChartStack";
import Metric from "../../components/Metric";
import ChartPie from "../../components/ChartPie";
import CharArena from "../../components/CharArena";
import TableOrder from "../../components/TableOrder";
import { Select, DatePicker } from "antd";
import * as Antd from 'antd';
import export_csv from '../../assets/images/dashboard/export_csv.svg'
import menu_dots from '../../assets/images/dashboard/menu_dots.svg'
import Search from "antd/es/input/Search";
import { SearchOutlined, SmileOutlined } from "@ant-design/icons";

const Dashboard: React.FC = () => {
	const dispatch = useAppDispatch();
	// const { account } = useWeb3React<Web3Provider>();
	const account = useAppSelector(getAddressWallet);
	// const { syncMyAsset } = useMutationSyncMyAsset();
	const [height, setHeight] = useState(0);

	useEffect(() => {
		if (!height) setHeight(window.innerHeight);
		window.addEventListener("resize", () => {
			setHeight(window.innerHeight - 100);
		});
	}, [height]);

	const items: Antd.MenuProps['items'] = [
		{
			key: '1',
			label: (
				<a target="_blank"  >
					1st menu item
				</a>
			),
		},
		{
			key: '2',
			label: (
				<a target="_blank"  >
					2nd menu item (disabled)
				</a>
			),
			icon: <SmileOutlined />,
			disabled: true,
		},
		{
			key: '3',
			label: (
				<a target="_blank" >
					3rd menu item (disabled)
				</a>
			),
			disabled: true,
		},
		{
			key: '4',
			danger: true,
			label: 'a danger item',
		},
	];

	return (
		<React.Fragment>
			<div className="col col-content" id="myItemScrollAble" style={{ height: height, overflow: "auto" }}>
				<main className="main-content">
					<div className="header-toolbar pt-4 pb-4">
						<h1 className="title-toolbar h2">DashBoard</h1>
						<div className="navbar-menu" >
							<div className="btn-navbar d-flex">
								<div className="d-flex ms-1">
									<Antd.Button className="mr-4" >
										<img src={export_csv} className='mr-4' />
										Xuất File
									</Antd.Button>
									<div>
										<DatePicker.RangePicker className="mr-4" />
									</div>

									<Select
										defaultValue="lucy"
										style={{ width: 120 }}
										// onChange={handleChange}
										options={[
											{ value: 'day', label: 'Day' },
											{ value: 'lucy', label: 'Week' },
											{ value: 'Yiminghe', label: 'Month' },
										]}
									/>
								</div>
							</div>
						</div>
					</div>
					<Row>
						<Col className="item-col" xs={12} md={6} lg={4} xl={4}>
							<Metric title='Total Complete Order' type={1} />
						</Col>
						<Col className="item-col" xs={12} md={6} lg={4} xl={4}>
							<Metric title='Total Volume' type={2} />
						</Col>
						<Col className="item-col" xs={12} md={6} lg={4} xl={4}>
							<Metric title='Partner Fee' type={3} />
						</Col>
					</Row>
					<div className="card-box">
						<div className="view-alert">
							<div className="header-toolbar pt-4 pb-4">
								<h3 className="title-toolbar">Total Complete Order</h3>
							</div>
							<ChartStack />
						</div>
					</div>
					<Row>
						<Col className="item-col" xs={12} md={6} lg={4} xl={4}>
							<div className="card-box">
								<div className="view-alert">
									<div className="header-toolbar pt-4 pb-4">
										<h3 className="title-toolbar">Total Volume</h3>
									</div>
									<ChartPie />
								</div>
							</div>
						</Col>
						<Col className="item-col" xs={12} md={6} lg={4} xl={8}>
							<div className="card-box">
								<div className="view-alert">
									<div className="header-toolbar pt-4 pb-4">
										<h3 className="title-toolbar">Total Complete Order</h3>
									</div>
									<ChartStack />
								</div>
							</div>
						</Col>
					</Row>
					<div className="card-box">
						<div className="header-toolbar pt-4 pb-4">
							<h3 className="title-toolbar">Partner fees</h3>
						</div>
						<div className="view-alert">


						</div>
						<CharArena />
					</div>
					<div className="card-box">
						<div className="view-alert">
							<div className="header-toolbar pt-4 pb-4">
								<h4 className="title-toolbar">Order</h4>
								<div className="btn-navbar d-flex">
									<div className="d-flex ms-1">
										<Antd.Input addonBefore={<SearchOutlined />} placeholder="input search text" style={{ width: 304 }} className="mr-4" />

										<Select
											defaultValue="lucy"
											style={{ width: 120 }}
											placeholder='product type'
											className="mr-4"
											options={[
												{ value: 'day', label: 'Day' },
												{ value: 'lucy', label: 'Week' },
												{ value: 'Yiminghe', label: 'Month' },
											]}
										/>
										<Select
											defaultValue="lucy"
											style={{ width: 120 }}
											placeholder='token'
											className="mr-4"
											options={[
												{ value: 'day', label: 'Day' },
												{ value: 'lucy', label: 'Week' },
												{ value: 'Yiminghe', label: 'Month' },
											]}
										/>
										<DatePicker.RangePicker className="mr-4" />
										<Antd.Dropdown menu={{ items }} trigger={['click']}>
											<Antd.Button danger type="text">
												<img src={menu_dots} />
											</Antd.Button>
										</Antd.Dropdown>

									</div>
								</div>
							</div>

							<div className="header-toolbar" >
								<TableOrder />
							</div>
						</div>
					</div>

				</main>
			</div>
		</React.Fragment>
	);
};

export default Dashboard;
