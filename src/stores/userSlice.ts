import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { RootState } from ".";

interface User {
	email?: string;
	is_verify?: boolean;
	username?: string;
	avatar?: string;
	nonce?: number;
	expired: number;
	is_qr?: boolean;
}
interface UserState {
	address: string;
	signature: string;
	user?: User;
	cvtBalance: string;
	bnbBalance: string;
	ethBalance: string;
	usdtBalance: string;
	usdBalance: number;
}

const address = localStorage.getItem("address");
const signature = localStorage.getItem("signature");
const user = localStorage.getItem("user");

const initialSate: UserState = {
	address: address ? address : "",
	signature: signature ? signature : "",
	user: user ? JSON.parse(user) : {},
	cvtBalance: "0",
	bnbBalance: "0",
	ethBalance: "0",
	usdtBalance: "0",
	usdBalance: 0,
};

export const userSlice = createSlice({
	name: "user",
	initialState: initialSate,
	reducers: {
		loggined: (
			state,
			action: PayloadAction<{
				address: string;
				signature: string;
				user: User;
			}>,
		) => {
			state.address = action.payload.address;
			state.signature = action.payload.signature;
			state.user = action.payload.user;

			localStorage.setItem("signature", state.signature);
			localStorage.setItem("address", state.address);
			localStorage.setItem("user", JSON.stringify(state.user));
		},
		logout: state => {
			state.address = "";
			state.signature = "";
			state.user = undefined;
			localStorage.removeItem("address");
			localStorage.removeItem("signature");
			localStorage.removeItem("user");
		},
		updateCVTBalance: (state, action: PayloadAction<string>) => {
			state.cvtBalance = action.payload;
		},
		updateBNBBalance: (state, action: PayloadAction<string>) => {
			state.bnbBalance = action.payload;
		},
		updateETHBalance: (state, action: PayloadAction<string>) => {
			state.ethBalance = action.payload;
		},
		updateUSDTBalance: (state, action: PayloadAction<string>) => {
			state.usdtBalance = action.payload;
		},
	},
});

export const { loggined, logout, updateCVTBalance, updateBNBBalance, updateETHBalance, updateUSDTBalance } =
	userSlice.actions;
export const getUSer = (state: RootState) => state.user.user;
export const getCVTBalance = (state: RootState) => state.user.cvtBalance;
export const getBNBBalance = (state: RootState) => state.user.bnbBalance;
export const getETHBalance = (state: RootState) => state.user.ethBalance;
export const getUSDTBalance = (state: RootState) => state.user.usdtBalance;

export const getUserSignature = (account: any) => (state: RootState) =>
	state.user.address === account ? state.user.signature : "";

export const getAddressWallet = (state: RootState) => state.user.address;

export default userSlice.reducer;
