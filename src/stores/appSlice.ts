import { createSlice, PayloadAction } from "@reduxjs/toolkit";
import { RootState } from ".";
import { init } from "fbt";
import moment from "moment";

interface Language {
	countryCode: string;
	languageCode: string;
	languageTag: string;
}

const LOCATES: { [key: string]: Language } = {
	vi: { countryCode: "VN", languageCode: "vi", languageTag: "vi-VN" },
	en: { countryCode: "US", languageCode: "en", languageTag: "en-US" },
};

const moment_locales: { [key: string]: any } = {
	vi: () => require("moment/locale/vi"),
	en: () => require("moment/locale/en-gb"),
};

const setLanguage = (languageCode: string, languageTag: string) => {
	const viewerContext = {
		locale: languageTag,
	};
	init({
		translations: require("./translate.json"),
		hooks: {
			getViewerContext: () => viewerContext,
		},
	});
	moment_locales[languageCode]();
	moment.locale(languageCode);
};

let currentLeague = localStorage.getItem("lang");
if (currentLeague === null) {
	currentLeague = "en";
}

const language = LOCATES[currentLeague] ? LOCATES[currentLeague] : LOCATES["en"];

interface AppState {
	isInited: boolean;
	lang: Language;
	state: "NOT_LOGIN" | "LOGINING" | "LOGINED" | "LOGINING_QRCODE";
}

const initialState: AppState = {
	isInited: false,
	lang: language,
	state: "NOT_LOGIN",
};

export const appSlice = createSlice({
	name: "app",
	initialState: initialState,
	reducers: {
		inited: state => {
			state.isInited = true;
			setLanguage(language.languageCode, language.languageTag);
			localStorage.setItem("lang", language.languageCode);
		},
		changeLanguage: (state, action: PayloadAction<string>) => {
			if (LOCATES[action.payload]) {
				state.lang = LOCATES[action.payload];
				setLanguage(LOCATES[action.payload].languageCode, LOCATES[action.payload].languageTag);
				localStorage.setItem("lang", LOCATES[action.payload].languageCode);
			}
		},
		updateState: (state, action: PayloadAction<"NOT_LOGIN" | "LOGINING" | "LOGINED" | "LOGINING_QRCODE">) => {
			state.state = action.payload;
		},
	},
});

export const { inited, updateState } = appSlice.actions;

export const isInited = (state: RootState) => state.app.isInited;

export default appSlice.reducer;
