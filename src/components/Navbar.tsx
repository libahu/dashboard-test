import React from "react";
import { Button, Navbar } from "react-bootstrap";
import { NavLink, useHistory, useLocation } from "react-router-dom";
import logo_cv from "../assets/images/dashboard/logo_cv.svg";
import nav_profile from "../assets/images/dashboard/nav_profile.svg";
import logout from "../assets/images/dashboard/logout.svg"
import { useAppSelector, useAppDispatch } from "../stores/hooks";

const NavbarView: React.FC = () => {
	const dispatch = useAppDispatch();
	const location = useLocation();
	const history = useHistory();

	const app = useAppSelector(state => state.app);
	const handleClick = () => {
		history.push('/login');
	}

	const renderAction = () => (
		<div className="d-flex ms-1">
			<div className="dropdown">
				<h5 className="">ChainVerse Partner</h5>
				<p className="d-0">ChainversePartner@gmail.com</p>
			</div>
			<div className="dropdown">
				<Button
					className="btn btn-icon-only"
					type="button"
					id="dropdownMenuProfile"
					data-bs-toggle="dropdown"
					aria-expanded="false"
					onClick={()=>handleClick()}
				>
					<img src={logout} />
				</Button>
			</div>
		</div>
	);


	return (
		<Navbar bg="light" expand="lg" className="view-navbar sticky-top flex-md-nowrap">
			<Navbar.Brand href="/" className="me-0 px-3">
				<img src={logo_cv} className="brand-logo" />
			</Navbar.Brand>
			<Navbar.Toggle aria-controls="navbarMenu" />
			<Navbar.Collapse id="navbarMenu">
				<div className="navbar-menu">
					<div className="navbar-tab">
						<ul className="navbar-nav me-auto mb-2 mb-lg-0">
						</ul>
					</div>

					<div className="btn-navbar d-flex">
						{renderAction()}
					</div>
				</div>
			</Navbar.Collapse>
		</Navbar>
	);
};

export default NavbarView;
