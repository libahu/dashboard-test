import React, { useState, useEffect } from 'react';
import ReactDOM from 'react-dom';
import { Column } from '@ant-design/plots';

const ChartStack = () => {
	const [data, setData] = useState([]);

	useEffect(() => {
		asyncFetch();
	}, []);

	const asyncFetch = () => {
		fetch('https://gw.alipayobjects.com/os/antfincdn/8elHX%26irfq/stack-column-data.json')
			.then((response) => response.json())
			.then((json) => setData(json))
			.catch((error) => {
				console.log('fetch data failed', error);
			});
	};
	const config = {
		data,
		isStack: true,
		xField: 'year',
		yField: 'value',
		seriesField: 'type',
		// label: {
		// 	position: 'middle', // 'top', 'bottom', 'middle'
		// },
		interactions: [
			{
				type: 'active-region',
				enable: false,
			},
		],
		connectedArea: {
			style: (oldStyle: any, element: any) => {
				return {
					fill: 'rgba(0,0,0,0.25)',
					stroke: oldStyle.fill,
					lineWidth: 0.5,
				};
			},
		},
	};

	return <Column
		// data={config.data}
		// xField={config.xField}
		// yField={config.yField}
		// // label={config.label}
		// isStack={true}
		// seriesField='type'
		// connectedArea={config.connectedArea}
		{...config}
	/>;
};

export default ChartStack
