import React from 'react';
import { Pie, measureTextWidth } from '@ant-design/plots';

const ChartPie = () => {
	function renderStatistic(containerWidth: any, text: any, style: any) {
		const { width: textWidth, height: textHeight } = measureTextWidth(text, style);
		const R = containerWidth / 2; 

		let scale = 1;

		if (containerWidth < textWidth) {
			scale = Math.min(Math.sqrt(Math.abs(Math.pow(R, 2) / (Math.pow(textWidth / 2, 2) + Math.pow(textHeight, 2)))), 1);
		}

		const textStyleStr = `width:${containerWidth}px;`;
		return `<div style="${textStyleStr};font-size:${scale}em;line-height:${scale < 1 ? 1 : 'inherit'};">${text}</div>`;
	}

	const data = [
		{
			type: '1',
			value: 27,
		},
		{
			type: '2',
			value: 25,
		},
		{
			type: '3',
			value: 18,
		},
		{
			type: '4',
			value: 15,
		},
		{
			type: '5',
			value: 10,
		},
		{
			type: '6',
			value: 5,
		},
	];

	const config = {
		appendPadding: 10,
		data,
		angleField: 'value',
		colorField: 'type',
		radius: 1,
		innerRadius: 0.64,
		meta: {
			value: {
				formatter: (v: any) => `${v} VND`,
			},
		},
		label: {
			type: 'inner',
			offset: '-50%',
			style: {
				textAlign: 'center',
			},
			autoRotate: false,
			content: '{value}',
		},
		statistic: {
			title: {
				offsetY: -4,
				customHtml: (container: any, view: any, datum: any) => {
					const { width, height } = container.getBoundingClientRect();
					const d = Math.sqrt(Math.pow(width / 2, 2) + Math.pow(height / 2, 2));
					const text = datum ? datum.type : 'Sum';
					return renderStatistic(d, text, {
						fontSize: 28,
					});
				},
			},
			content: {
				offsetY: 4,
				style: {
					fontSize: '32px',
				},
				customHtml: (container: any, view: any, datum: any, data: any) => {
					const { width } = container.getBoundingClientRect();
					const text = datum ? `${datum.value} VND` : `${data.reduce((r: any, d: any) => r + d.value, 0)} VND`;
					return renderStatistic(width, text, {
						fontSize: 32,
					});
				},
			},
		},
		interactions: [
			{
				type: 'element-selected',
			},
			{
				type: 'element-active',
			},
			{
				type: 'pie-statistic-active',
			},
		],
	};

	return <Pie {...config} />;
};


export default ChartPie