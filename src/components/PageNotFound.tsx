import React from "react";
import data_empty from "../assets/images/default/Empty_Inbox.png";

const PageNotFound: React.FunctionComponent = () => {
	return (
		<div className="view-empty">
			<div className="empty-cover">
				<img src={data_empty} />
			</div>
			<div className="empty-header">Page not found</div>
			<div className="empty-title">We can’t find any pages matching your search</div>
		</div>
	);
};

export default PageNotFound;
