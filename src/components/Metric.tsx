import React, { useState, useEffect } from 'react';
import ReactDOM from 'react-dom';
import { Column } from '@ant-design/plots';
import metric_1 from '../assets/images/dashboard/metric_1.svg'
import metric_2 from '../assets/images/dashboard/metric_2.svg'
import metric_3 from '../assets/images/dashboard/metric_3.svg'
import up from '../assets/images/dashboard/Arrow_Up.svg'
import down from '../assets/images/dashboard/Arrow_Down.svg'
interface IProps {
	title: string
	type: number
}

const Metric = (props: IProps) => {
	return (
		<React.Fragment>
			<div className="card-box pb-0">
				<div className="view-alert">
					<div className="header-toolbar">
						<h4 className="title-toolbar test-1">{props.title}</h4>
						<div className="d-flex ms-1">
							{/* <img src={metric_1} /> */}
							{props.type == 1 && <img src={metric_1} />}
							{props.type == 2 && <img src={metric_2} />}
							{props.type == 3 && <img src={metric_3} />}
						</div>
					</div>
					<h1 className='mt-8' >100</h1>
					<div className='mt-4'>
						{props.type == 1 && (
							<>
								<img src={down} width="10%" height="10%" />
								<div className='matric-color-down'>15%</div>
							</>
						)}
						{props.type == 2 && (
							<>
								<img src={up} width="10%" height="10%" />
								<div className='matric-color-up'>20%</div>
							</>
						)}
						{props.type == 3 && (
							<>
								<img src={up} width="10%" height="10%" />
								<div className="matric-color-up">25%</div>
							</>
						)}
					</div>

				</div>
			</div>
		</React.Fragment>
	)

};

export default Metric
