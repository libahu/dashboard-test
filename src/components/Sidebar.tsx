import React from "react";
import { Form, ListGroup } from "react-bootstrap";
import { NavLink, useLocation } from "react-router-dom";
import ic_items from "../assets/images/nav/ic_items.svg";
import ic_items_active from "../assets/images/nav/ic_items_active.svg";
import ic_bids from "../assets/images/nav/ic_bids.svg";
import ic_bids_active from "../assets/images/nav/ic_bids_active.svg";
import ic_games from "../assets/images/nav/ic_games.svg";
import ic_games_active from "../assets/images/nav/ic_games_active.svg";
import ic_history from "../assets/images/nav/ic_history.svg";
import ic_history_active from "../assets/images/nav/ic_history_active.svg";
import { useAppDispatch } from "../stores/hooks";

interface IProps {
	itemCount?: number;
}

const Sidebar: React.FC<IProps> = props => {

	const dispatch = useAppDispatch();

	const checkActive = (match: any, location: { pathname: any }) => {
		//some additional logic to verify you are in the home URI
		if (!location) return false;
		const { pathname } = location;
		// console.log(pathname);
		return pathname === "/";
	};

	const location = useLocation();

	return (
		<nav id="sidebarMenu" className="col col-sidebar d-md-block sidebar">
			<div className="position-sticky pt-3">
				<div className="bx-block bx-filters">
					<div className="header-bx">
						<h4 className="title-text" >Partner Dashboard</h4>
					</div>
					<div className="content-bx">
						<Form.Group className="form-group mb-4" controlId="formStatus">
							{/* <Form.Label>My Assets</Form.Label> */}
							<ListGroup variant="flush" className="my-2">
								<ListGroup.Item active={location.pathname.startsWith("/dashboard") ? true : false}>
									<NavLink className="with-badge" to="/dashboard" activeClassName="active" isActive={checkActive}>
										<div className="left">
											<img
												src={location.pathname.startsWith("/dashboard") ? ic_items_active : ic_items}
												className="item-icon"
											/>{" "}
											Dashboard
										</div>
										{/* <div className="right">{itemsCount || 0}</div> */}
									</NavLink>
								</ListGroup.Item>

								<ListGroup.Item active={location.pathname.startsWith("/setting") ? true : false}>
									<NavLink
										className="with-badge"
										exact
										to="/setting"
										activeClassName="active"
										isActive={checkActive}
									>
										<div className="left">
											<img
												src={location.pathname.startsWith("/setting") ? ic_games_active : ic_games}
												className="item-icon"
											/>{" "}
											Setting
										</div>
										{/* <div className="right">{gamesCount || 0}</div> */}
									</NavLink>
								</ListGroup.Item>
								<ListGroup.Item active={location.pathname.startsWith("/development") ? true : false}>
									<NavLink to="/development">
										<div className="left">
											<img
												src={location.pathname.startsWith("/development") ? ic_bids_active : ic_bids}
												className="item-icon"
											/>{" "}
											Development
										</div>
									</NavLink>
								</ListGroup.Item>
							</ListGroup>
						</Form.Group>
					</div>
				</div>
			</div>
		</nav>
	);
};

export default Sidebar;
