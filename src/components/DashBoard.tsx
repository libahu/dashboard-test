import React from "react";
import { Container, Row } from "react-bootstrap";
import { Switch, Route, withRouter } from "react-router-dom";
import Sidebar from "./Sidebar";
import { useAppSelector } from "../stores/hooks";
import Dashboard from "../pages/dashboard";
import Navbar from "./Navbar";
import Setting from "../pages/setting";
import Development from "../pages/development";

const Account: React.FunctionComponent = () => {
	const app = useAppSelector(state => state.app);

	return (
		<>
			<Container fluid className="view-container">
				<Row className="row-page page-account">
					<Navbar />
					<Switch>
						<Route>
							<Sidebar />
							<Switch>
								<Route exact path="/dashboard" component={Dashboard} />
								<Route exact path="/setting" component={Setting} />
								<Route exact path="/development" component={Development} />
							</Switch>
						</Route>
					</Switch>

				</Row>
			</Container>
		</>
	);
};

export default withRouter(Account);
