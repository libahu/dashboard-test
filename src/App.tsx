import "./assets/scss/main.scss";
import PageNotFound from "./components/PageNotFound";
import React from "react";
import { Route, Switch, Redirect } from "react-router-dom";
import "./App.css";
import Dashboard from "./components/DashBoard";
import LoginView from "./pages/Auth/Login";

const App: React.FC = () => {
	// const dispatch = useDispatch();
	// const app = useAppSelector(state => state.app);

	return (
		<>
			<div className="view-page">
				<Switch>
					<Route exact path="/">
						<Redirect to="/dashboard" />
					</Route>
					<Route exact path="/dashboard" component={Dashboard} />
					<Route exact path="/setting" component={Dashboard} />
					<Route exact path="/development" component={Dashboard} />
					<Route exact path="/login" component={LoginView} />
					<Route>
						<PageNotFound />
					</Route>
				</Switch>
			</div>
		</>
	);
};

export default React.memo(App);
